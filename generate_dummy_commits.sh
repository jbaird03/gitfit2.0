#!/bin/bash

# Initialize a Git repository
git init

# Create dummy files
touch file1.txt file2.py file3.md

# Add and commit initial files
git add .
git commit -m "Initial commit: Adding dummy files"

# Define the number of lines in the file with random code snippets
NUM_LINES=$(wc -l < random_code_snippets.txt)

# Get the start date of the last month
START_OF_LAST_MONTH=$(date -d "$(date +%Y-%m-01) -1 month" +%Y-%m-01)

# Define the number of commits to create
NUM_COMMITS=25

# Loop to create additional dummy commits
for ((i = 1; i <= NUM_COMMITS; i++)); do
    # Randomly select lines from the file with code snippets
    RANDOM_LINES=$(shuf -n $((RANDOM % NUM_LINES + 1)) random_code_snippets.txt)
    # Append randomly selected lines to file1.txt
    echo "$RANDOM_LINES" >> file1.txt
    # Add changes and commit
    git add file1.txt
    COMMIT_DATE=$(date -d "$START_OF_LAST_MONTH +$i days" +"%Y-%m-%d %H:%M:%S")
    GIT_COMMITTER_DATE="$COMMIT_DATE" git commit --date="$COMMIT_DATE" -m "Dummy commit $i: Make changes to file1.txt"
done

# View commit history
git log
